# agdgmakesgame

## Organization

### Trello page

https://trello.com/b/cvwQ7xTr/agdg-homeless-simulator

### Ad-hoc chatroom thing

https://tlk.io/agdgmakesgame

### Contact

 Collaborator | Role         		     | E-mail                   | Timezones
--------------|---------------------|--------------------------|-------------------------------------------
 cogloch      | Benevolent overlord | cogloch@outlook.com      | GMT +3
 Gooseheaded  | Raging code baboon  | goosehead101@hotmail.com | GMT -6
 Soph93       | Shitposter 	 		     | admin@meatspin.com (just kidding, no idea what this guy's e-mail is) |
 Fridge/D     | Art slave           | A0Refrigerator@gmail.com | GMT +10

## Politics

 Category | Result        | Poll Link
----------|---------------|-------------
 Genre    | Survival      | http://strawpoll.me/5138540/r
 Anime    | No            | http://strawpoll.me/5138619/r
 Theme    | Homeless      | http://strawpoll.me/5139063/r
 Setting  | 'Murica       | http://strawpoll.me/5139417/r
 Camera   | 3D minimalist | http://strawpoll.me/5139266/r

## Coding standard

Concept      | Consensus
-------------|-----------
Casing       | Pascal case for files, classes, and functions. (e.g. *Game::InitSystems()*). Camel case for everything else. (e.g. *posX, posY*)
Global vars  | Global variables have a g_ prefix. (e.g. *g_myGlobalObject*)
Member vars  | Class members have an m_ prefix. (e.g. *m_cash*)
Static vars  | Static variables an s_ prefix. (e.g. *s_broadcasterIstantiated*)
Pointers     | Pointer variables have a p prefix. (e.g. *pMyPtr*)
Braces       | Braces always go on the next line. Every code block must be present in braces -- even if they are only 1 line long.

### Comment annotations
* **TODO**: Small issues that do not deserve a git issue/trello item.
* **TO-REMOVE**: Temporary notes that should be removed after reading by the rest of the team.
* **FIXME**: Problems that have been identified, but not yet fixed, and are annotated as to not be forgotten.
