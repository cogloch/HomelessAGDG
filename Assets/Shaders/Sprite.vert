#version 430 core
layout (location = 0) in vec2 position;
layout (location = 1) in vec2 texCoordIn;

uniform mat4 model;
uniform mat4 proj;

out vec2 texCoord;

void main()
{
    gl_Position = proj * model * vec4(position, 0.0, 1.0);
	
	texCoord = vec2(texCoordIn.x, texCoordIn.y);
}