#version 430 core
 

// Per-vertex
layout (location = 0) in vec3 position;
layout (location = 1) in vec4 color;
out vec4 colorVert;

// Per-object
uniform mat4 model;

// Per-frame
uniform mat4 viewProj;


void main()
{
	gl_Position = viewProj * model * vec4(position, 1.0f);
	colorVert = color;
}

