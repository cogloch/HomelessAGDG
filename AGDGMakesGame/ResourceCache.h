#pragma
#include "Quad.h"
#include "Mesh.h"
#include "Texture.h"
#include "ShaderProgram.h"


// TODO:
// Resource packages reading/writing
// Reading/writing meshes in binary format(from obj)
class ResourceCache
{
public:
	ResourceCache();
	// Initialization is done outside the constructor since creating Opengl resources requires a working context
	// i.e. initialization is order-dependent.
	void Init();

	void LoadAllShaders();
	ResourceHandle LoadShader(const std::string&);
	ShaderProgram& GetShader(ResourceHandle);
	ShaderProgram& GetShader(const std::string&);
	ResourceHandle GetShaderHandle(const std::string&);

	void LoadAllMeshes();
	ResourceHandle LoadMesh(const std::string&);
	Mesh& GetMesh(ResourceHandle);
	Mesh& GetMesh(const std::string&);
	ResourceHandle GetMeshHandle(const std::string&);

	void LoadAllTextures();
	ResourceHandle LoadTexture(const std::string&);
	Texture& GetTexture(ResourceHandle);
	Texture& GetTexture(const std::string&);
	ResourceHandle GetTextureHandle(const std::string&);

	Quad& GetQuad();

	const std::string& GetMeshesPath() const;
	const std::string& GetShadersPath() const;
	const std::string& GetTexturesPath() const;
	const std::string& GetUILayoutsPath() const;

private:
	std::vector<Mesh> m_meshes;
	std::vector<Texture> m_textures;
	std::vector<ShaderProgram> m_shaders;

	// TODO: String hashing for resource names
	std::map<std::string, ResourceHandle> m_meshNames;
	std::map<std::string, ResourceHandle> m_shaderNames;
	std::map<std::string, ResourceHandle> m_textureNames;
	
	void SolveAbsolutePaths();
	std::string m_assetsPath;
	std::string m_meshesPath;
	std::string m_shadersPath;
	std::string m_texturesPath;
	std::string m_uiLayoutsPath;

	Quad m_quad;

	static bool s_bInstantiated;
};

extern ResourceCache g_resourceCache;

inline const std::string& ResourceCache::GetUILayoutsPath() const
{
	return m_uiLayoutsPath;
}

inline const std::string& ResourceCache::GetShadersPath() const
{
	return m_shadersPath;
}

inline const std::string& ResourceCache::GetMeshesPath() const
{
	return m_meshesPath;
}

inline const std::string& ResourceCache::GetTexturesPath() const
{
	return m_texturesPath;
}

inline Mesh& ResourceCache::GetMesh(ResourceHandle handle)
{
	return m_meshes[handle];
}

inline Mesh& ResourceCache::GetMesh(const std::string& name)
{
	return m_meshes[m_meshNames[name]];
}

inline ResourceHandle ResourceCache::GetMeshHandle(const std::string& name)
{
	return m_meshNames[name];
}

inline ShaderProgram& ResourceCache::GetShader(ResourceHandle handle)
{
	return m_shaders[handle];
}

inline ShaderProgram& ResourceCache::GetShader(const std::string& name)
{
	return m_shaders[m_shaderNames[name]];
}

inline ResourceHandle ResourceCache::GetShaderHandle(const std::string& name)
{
	return m_shaderNames[name];
}

inline Texture& ResourceCache::GetTexture(ResourceHandle handle)
{
	return m_textures[handle];
}

inline Texture& ResourceCache::GetTexture(const std::string& name)
{
	return m_textures[m_textureNames[name]];
}

inline ResourceHandle ResourceCache::GetTextureHandle(const std::string& name)
{
	return m_textureNames[name];
}

inline Quad& ResourceCache::GetQuad()
{
	return m_quad;
}