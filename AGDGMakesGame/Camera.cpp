#include "PCH.h"
#include "Camera.h"


Camera::Camera()
{
	CacheViewMatrix();
}

Camera::Camera(TransformNoScale& transform) 
	: m_transform(transform)
{
	CacheViewMatrix();
}

void Camera::Update(const TransformNoScale& transform)
{
	if (transform != m_transform)
	{
		m_transform = transform;
		CacheViewMatrix();
	}
}

void Camera::CacheViewMatrix()
{
	auto posVec = m_transform.GetPosition();
	auto translateMat = glm::translate(glm::mat4(1.0f), glm::vec3(-posVec.x, -posVec.y, -posVec.z));
	m_viewMatrix = translateMat * glm::toMat4(m_transform.GetRotationQuat());
}