#pragma once
#include "UILayout.h"
#include "EventHandler.h"


class Renderer;

class UIManager : public EventHandler
{
public:
	UIManager();
	void Init(Renderer*, const std::string& fullLayoutsPath);
	const UILayout& GetActiveLayout() const;
	void SetActiveLayout(const U32 layoutId); // TODO: Refer to layouts with (hashed) human readable strings

	void Handle(const Event&);

private:
	Renderer* m_pRenderer;
	U32 m_activeLayout;
	std::vector<UILayout> m_layouts;
	static bool s_bInstantiated;
};

inline const UILayout& UIManager::GetActiveLayout() const
{
	return m_layouts[m_activeLayout];
}

inline void UIManager::SetActiveLayout(const U32 layoutId)
{
	assert(layoutId < m_layouts.size());
	m_activeLayout = layoutId;
}