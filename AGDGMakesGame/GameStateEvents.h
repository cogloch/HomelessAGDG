#pragma once
#include "Event.h"
#include <utility>


Event&& CreateTickEvent(float dt)
{
	Event ev;
	ev.type = EventType::TICK;
	ev.length = 1;
	ev.data[0] = EventData(dt);

	return std::move(ev);
}