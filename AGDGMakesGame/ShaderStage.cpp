#include "PCH.h"
#include "ShaderStage.h"
#include "ResourceCache.h"


std::map<ShaderType, std::string> ShaderStage::s_GLSLExtensions = {
	{ ShaderType::VERTEX, ".vert" },			
	{ ShaderType::FRAGMENT, ".frag" },		 
	{ ShaderType::TESS_CONTROL, ".tesc" },    
	{ ShaderType::TESS_EVAL, ".tese" }, 
	{ ShaderType::GEOMERTRY, ".geom" },		
	{ ShaderType::COMPUTE, ".comp" }			
};

ShaderStage::ShaderStage(const std::string& name, ShaderType type, U32 program)
	: m_name(name)
	, m_type(type)
	, m_program(program)
	, m_shader(0)
	, m_source("")
{
	ReadShaderSource(g_resourceCache.GetShadersPath() + name + s_GLSLExtensions[type]);
	CreateShader();
}

ShaderStage::~ShaderStage()
{
	// TODO: Check if m_program and m_shader are valid
	gl::DetachShader(m_program, m_shader);
	gl::DeleteShader(m_shader);
}

ShaderStage::ShaderStage(ShaderStage&& other)
	: m_name(std::move(other.m_name))
	, m_type(std::move(other.m_type))
	, m_program(std::move(other.m_program))
	, m_shader(std::move(other.m_shader))
	, m_source(std::move(other.m_source))
{
}

void ShaderStage::ReadShaderSource(const std::string& filename)
{
	// TODO: Handle file reading somewhere else 
	std::ifstream file(filename, std::ios::in);
	if (file.fail())
	{
		throw FileNotFound(filename);
	}

	file.seekg(0, std::ios::end);
	m_source.resize((unsigned int)file.tellg());
	file.seekg(0, std::ios::beg);
	file.read(&m_source[0], m_source.size());
}

void ShaderStage::CreateShader()
{
	m_shader = gl::CreateShader(static_cast<GLenum>(m_type));

	auto shaderCodePtr = m_source.c_str();
	auto shaderCodeSize = static_cast<GLint>(m_source.size());

	gl::ShaderSource(m_shader, 1, &shaderCodePtr, &shaderCodeSize);
	gl::CompileShader(m_shader);

	GLint status = 0;
	gl::GetShaderiv(m_shader, gl::COMPILE_STATUS, &status);

	if (status == 0)
	{
		char buffer[512];
		gl::GetShaderInfoLog(m_shader, 512, nullptr, buffer);
		throw ShaderCompilationError(m_name, m_type, buffer);
	}
}