#pragma once
#include "Event.h"
#include <utility>


Event&& CreateKeyUpEvent(U32 key)
{
	Event ev;
	ev.type = EventType::KEY_UP;
	ev.length = 1;
	ev.data[0] = EventData(key);

	return std::move(ev);
}

Event&& CreateKeyDownEvent(U32 key)
{
	Event ev;
	ev.type = EventType::KEY_DOWN;;
	ev.length = 1;
	ev.data[0] = EventData(key);

	return std::move(ev);
}

Event&& CreateKeyCharEvent(U32 key)
{
	Event ev;
	ev.type = EventType::KEY_CHAR;
	ev.length = 1;
	ev.data[0] = EventData(key);

	return std::move(ev);
}