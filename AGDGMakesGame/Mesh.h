#pragma once
#include "Vertex.h"


class Mesh
{
public:
	Mesh();
	void Init(const std::string& filename); // TEMP: Supports only triangulated faces.
	void Init(const std::vector<Vertex>& vertices, const std::vector<U32>& indices);
	void Draw() const;

	std::vector<Vertex> vertices;
	std::vector<U32> indices;

private:
	U32 VAO, VBO, EBO;
};