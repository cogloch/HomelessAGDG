#pragma once
#include "TransformNoScale.h"


class Transform : public TransformNoScale
{
public:
	Transform();
	Transform(const glm::vec3& position);
	Transform(const glm::vec3& position, const glm::quat& rotation);
	Transform(const glm::vec3& position, float pitch, float yaw, float roll);
	Transform(const glm::vec3& position, const glm::quat& rotation, const glm::vec3& scale);

	const glm::vec3& GetScale() const;

	// TODO: Transform concatenation

	friend bool operator==(const Transform&, const Transform&);
	friend bool operator!=(const Transform&, const Transform&);

	// Absolute values
	void SetScale(float uniformScale);
	void SetScale(const glm::vec3&);
	void SetScale(float x, float y, float z);

	// Relative values
	void Scale(float byUniformScale);
	void Scale(const glm::vec3&);
	void Scale(float byX, float byY, float byZ);
	void ScaleX(float byX);
	void ScaleY(float byY);
	void ScaleZ(float byZ);

protected:
	void CacheTransform();

private:
	glm::vec3 m_scale;
};

inline const glm::vec3& Transform::GetScale() const
{
	return m_scale;
}