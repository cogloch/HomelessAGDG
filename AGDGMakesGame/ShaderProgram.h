#pragma once


class ShaderProgram
{
public:
	// Having an (optional) tesselation control shader implies having a tesselation evaluation shader also
	// @name - unique human readable name 
	ShaderProgram(const std::string& name,
				  bool bHasGeometryShader = false,
				  bool bHasTessControlShader = false,
				  bool bHasComputeShader = false);

	~ShaderProgram();

	// Non-copyable due to having a handle to an Opengl object which is released on deletion.
	ShaderProgram(ShaderProgram&);
	ShaderProgram& operator=(ShaderProgram&);

	// Returns a reference to an Opengl program object(the object's name)
	U32 GetProgram() const;

	// Returns the unique human-readable name the object was created with 
	const std::string& GetName() const;

private:
	void CreateProgram(bool bHasGeometryShader, bool bHasTessControlShader, bool bHasComputeShader);

private:
	// Reference to an Opengl shader program
	U32 m_program;

	// Unique shader name
	std::string m_name;

	// Makes sure the OGL shader program id is not released too early
	bool m_bReleaseable;
};

inline U32 ShaderProgram::GetProgram() const
{
	return m_program;
}

inline const std::string& ShaderProgram::GetName() const
{
	return m_name;
}

struct ShaderLinkingError : std::runtime_error
{
	ShaderLinkingError(const std::string& errorMsg) 
		: std::runtime_error("Shader linking error: " + errorMsg + "\n") {}
};