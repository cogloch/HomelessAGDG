#include "PCH.h"
#include "Window.h"
#include "EventBroadcaster.h"
#include "MouseEvents.h"
#include "KeyboardEvents.h"
#include "WindowEvents.h"


std::map<HWND, Window*> Window::s_windows = {};

Window::Window()
	: m_hWnd(0)
	, m_bShouldClose(false)
	, m_width(0)
	, m_height(0)
{
}

void Window::Init(HINSTANCE hInstance, U32 width, U32 height, const std::wstring& caption, bool bBorderless)
{
	assert(m_hWnd == 0);

	if (bBorderless)
	{
		m_windowStyle = WS_OVERLAPPED | WS_POPUP;
		m_positionX = 0;
		m_positionY = 0;
	}
	else
	{
		m_windowStyle = WS_OVERLAPPEDWINDOW | WS_VISIBLE;
		m_positionX = static_cast<U32>(CW_USEDEFAULT);
		m_positionY = static_cast<U32>(CW_USEDEFAULT);
	}

	WNDCLASSEX wc;
	memset(&wc, 0, sizeof(wc));

	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = &Window::WndProc;
	wc.cbClsExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
	wc.hIconSm = LoadIcon(nullptr, IDI_APPLICATION);
	wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName = nullptr;
	wc.lpszClassName = L"Window";

	RegisterClassEx(&wc);

	RECT rect;
	rect.top = rect.left = 0;
	rect.right = width;
	rect.bottom = height;

	AdjustWindowRectEx(&rect, m_windowStyle, false, 0);

	m_hWnd = CreateWindowEx(NULL,
							wc.lpszClassName,
							caption.c_str(),
							m_windowStyle,
							m_positionX,
							m_positionY,
							width,
							height,
							nullptr,
							nullptr,
							hInstance,
							nullptr);

	GetClientRect(m_hWnd, &rect);

	s_windows[m_hWnd] = this;

	ShowWindow(m_hWnd, SW_SHOWNORMAL);
	UpdateWindow(m_hWnd);
}

Window::~Window()
{
	s_windows.erase(m_hWnd);
}

LRESULT CALLBACK Window::WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	U32 xPosition = GET_X_LPARAM(lParam);
	U32 yPosition = GET_Y_LPARAM(lParam);

	switch (msg)
	{
		// Window Events
	case WM_CREATE:
		return 0;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_SIZE:
		if (s_windows.size())
		{
			g_eventBroadcaster.AddEvent(CreateWindowResizeEvent(LOWORD(lParam), HIWORD(lParam)));
			s_windows[hwnd]->m_width = LOWORD(lParam);
			s_windows[hwnd]->m_height = HIWORD(lParam);
		}
		break;

	// Mouse Events
	case WM_LBUTTONUP:
		g_eventBroadcaster.AddEvent(CreateLMouseUpEv(xPosition, yPosition));
		break;
	case WM_LBUTTONDOWN:
		g_eventBroadcaster.AddEvent(CreateLMouseDownEv(xPosition, yPosition));
		break;
	case WM_RBUTTONUP:
		g_eventBroadcaster.AddEvent(CreateRMouseUpEv(xPosition, yPosition));
		break;
	case WM_RBUTTONDOWN:
		g_eventBroadcaster.AddEvent(CreateRMouseDownEv(xPosition, yPosition));
		break;
	case WM_MOUSEMOVE:
		g_eventBroadcaster.AddEvent(CreateMouseMoveEv(xPosition, yPosition));
		break;
	case WM_MOUSEWHEEL:
		g_eventBroadcaster.AddEvent(CreateMouseWheelEv(GET_WHEEL_DELTA_WPARAM(wParam)));
		break;

		// Keyboard Events
	case WM_CHAR:
		g_eventBroadcaster.AddEvent(CreateKeyCharEvent(wParam));
		break;

	case WM_KEYUP:
		g_eventBroadcaster.AddEvent(CreateKeyUpEvent(wParam));
		break;

	case WM_KEYDOWN:
		g_eventBroadcaster.AddEvent(CreateKeyDownEvent(wParam));
		break;

	case WM_SYSCOMMAND:
		if (wParam == SC_CLOSE)
		{
			//g_eventBroadcaster.AddEvent({ Event::EngineEvents::EXIT });
		}
		break;
	}

	return DefWindowProc(hwnd, msg, wParam, lParam);
}

void Window::RunMessageLoop()
{
	MSG msg;

	while (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
	{
		if (msg.message == WM_QUIT)
		{
			m_bShouldClose = true;
			return;
		}

		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}