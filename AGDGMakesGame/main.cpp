#include "PCH.h"
#include "Game.h"

#ifdef WIN32
int WINAPI WinMain(_In_ HINSTANCE hInstance, 
				   _In_opt_ HINSTANCE, _In_ LPSTR, _In_ int)
{
	Game game(hInstance);
	return game.Run();
}
#else
int main()
{
	Game game;
	return game.Run();
}
#endif