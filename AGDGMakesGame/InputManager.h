#pragma once
#include "EventHandler.h"


// Holds state info for continuous input.
class InputManager : EventHandler
{
public:
	bool IsKeyPressed(unsigned char);
	bool IsLeftMousePressed();
	bool IsRightMousePressed();

	InputManager();
	~InputManager();
	void Handle(const Event&);

private:
	std::map<unsigned char, bool> m_keyStates;
	bool m_bPressedLBtn;
	bool m_bPressedRBtn;
	static bool s_bInstantiated;
};

extern InputManager g_inputManager;

inline bool InputManager::IsKeyPressed(unsigned char key)
{
	return m_keyStates[key];
}

inline bool InputManager::IsLeftMousePressed()
{
	return m_bPressedLBtn;
}

inline bool InputManager::IsRightMousePressed()
{
	return m_bPressedRBtn;
}
