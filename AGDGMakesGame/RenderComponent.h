#pragma once
#include "Transform.h"


class Renderer;

class RenderComponent
{
public:
	RenderComponent(Renderer*, const std::string& shaderName, const std::string& meshName);
	U32 GetRenderItem() const;

	Transform m_transform;

private:
	U32 m_renderItemId;
};

inline U32 RenderComponent::GetRenderItem() const
{
	return m_renderItemId;
}