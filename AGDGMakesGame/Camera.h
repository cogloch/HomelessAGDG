#pragma once
#include "TransformNoScale.h"
#include "Projection.h"


class Camera
{
public:
	Camera();
	Camera(TransformNoScale&);

	const glm::mat4& GetViewMatrix() const;
	const glm::mat4 GetViewProjMatrix(const glm::mat4& projectionMat) const;
	const glm::mat4 GetViewProjMatrix(const Projection&) const;

	void Update(const TransformNoScale&);

private:
	void CacheViewMatrix();

	TransformNoScale m_transform;
	glm::mat4 m_viewMatrix;
};

inline const glm::mat4& Camera::GetViewMatrix() const
{
	return m_viewMatrix;
}

inline const glm::mat4 Camera::GetViewProjMatrix(const glm::mat4& projectionMat) const
{
	return projectionMat * m_viewMatrix;
}

inline const glm::mat4 Camera::GetViewProjMatrix(const Projection& projection) const
{
	return projection.GetMatrix() * m_viewMatrix;
}