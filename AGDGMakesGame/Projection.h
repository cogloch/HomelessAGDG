#pragma once


class Projection
{
public:
	const glm::mat4& GetMatrix() const;

protected:
	Projection(float viewWidth, float viewHeight, float nearPlane, float farPlane);

	virtual void CacheMatrix() = 0;
	glm::mat4 m_matrix;

	float m_viewWidth;
	float m_viewHeight;
	float m_fovY;
	float m_nearPlane;
	float m_farPlane;
};

class OrthoProjection : public Projection
{
public:
	OrthoProjection(float viewWidth, float viewHeight, float nearPlane = -1.0f, float farPlane = 1.0f);

protected:
	void CacheMatrix();
};

class PerspectiveProjection : public Projection
{
public:
	PerspectiveProjection(float viewWidth, float viewHeight, float fovY, float nearPlane = 0.1f, float farPlane = 100.0f);
	
protected:
	void CacheMatrix();
	float m_fovY;
};


inline const glm::mat4& Projection::GetMatrix() const
{
	return m_matrix;
}