#include "PCH.h"
#include "InputManager.h"


bool InputManager::s_bInstantiated = false;

InputManager::InputManager()
{
	assert(!s_bInstantiated);
	s_bInstantiated = true;

	m_bPressedLBtn = false;
	m_bPressedRBtn = false;
}

InputManager::~InputManager()
{
	s_bInstantiated = false;
}

void InputManager::Handle(const Event& ev)
{
	switch (ev.type)
	{
	case EventType::KEY_UP:
		m_keyStates[ev.data[0].asU32] = false;
		break;
	case EventType::KEY_DOWN:
		m_keyStates[ev.data[0].asU32] = true;
		break;

	case EventType::LMOUSE_UP:
		m_bPressedLBtn = false;
		break;
	case EventType::LMOUSE_DOWN:
		m_bPressedLBtn = true;
		break;

	case EventType::RMOUSE_UP:
		m_bPressedRBtn = false;
		break;
	case EventType::RMOUSE_DOWN:
		m_bPressedRBtn = true;
		break;
	}
}