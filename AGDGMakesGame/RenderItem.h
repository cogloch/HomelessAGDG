#pragma once
#include "Mesh.h"
#include "Vertex.h"
#include "Projection.h"


class RenderItem
{
public:
	RenderItem(ResourceHandle shader, ResourceHandle mesh);
	void Render(const glm::mat4& viewProjMat) const;
	void SetTransform(const glm::mat4&);

private:
	ResourceHandle m_mesh;
	ResourceHandle m_shader;

	glm::mat4 m_transform;
};