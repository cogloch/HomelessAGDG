#include "PCH.h"
#include "Quad.h"


void Quad::Create()
{
	Vertex2DTexture verts[] = {
		{ { 0.0f, 1.0f }, { 0.0f, 1.0f } },
		{ { 1.0f, 0.0f }, { 1.0f, 0.0f } },
		{ { 0.0f, 0.0f }, { 0.0f, 0.0f } },
		{ { 0.0f, 1.0f }, { 0.0f, 1.0f } },
		{ { 1.0f, 1.0f }, { 1.0f, 1.0f } },
		{ { 1.0f, 0.0f }, { 1.0f, 0.0f } }
	};

	gl::GenVertexArrays(1, &VAO);
	gl::GenBuffers(1, &VBO);


	gl::BindBuffer(gl::ARRAY_BUFFER, VBO);
	gl::BufferData(gl::ARRAY_BUFFER, sizeof(verts), verts, gl::STATIC_DRAW);

	gl::BindVertexArray(VAO);
	gl::EnableVertexAttribArray(0);
	gl::VertexAttribPointer(0, 2, gl::FLOAT, gl::FALSE_, sizeof(Vertex2DTexture), (GLvoid*)0);
	gl::EnableVertexAttribArray(1);
	gl::VertexAttribPointer(1, 2, gl::FLOAT, gl::FALSE_, sizeof(Vertex2DTexture), (GLvoid*)(offsetof(Vertex2DTexture, texCoord)));
	gl::BindBuffer(gl::ARRAY_BUFFER, 0);
	gl::BindVertexArray(0);
}

void Quad::Draw()
{
	gl::BindVertexArray(VAO);
	gl::DrawArrays(gl::TRIANGLES, 0, 6);
	gl::BindVertexArray(0);
}