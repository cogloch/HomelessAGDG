#include "PCH.h"
#include "ShaderProgram.h"
#include "ShaderStage.h"


ShaderProgram::ShaderProgram(const std::string& name,
							 bool bHasGeometryShader, bool bHasTessControlShader, bool bHasComputeShader)
	: m_name(name)
	, m_program(0)
	, m_bReleaseable(true)
{
	CreateProgram(bHasGeometryShader, bHasTessControlShader, bHasComputeShader);
}

ShaderProgram::ShaderProgram(ShaderProgram& other)
{
	other.m_bReleaseable = false;
	m_name = other.m_name;
	m_program = other.m_program;
}

ShaderProgram& ShaderProgram::operator=(ShaderProgram& other)
{
	other.m_bReleaseable = false;
	m_name = other.m_name;
	m_program = other.m_program;
	return *this;
}

ShaderProgram::~ShaderProgram()
{
	if (m_bReleaseable && gl::IsProgram(m_program))
	{
		gl::DeleteProgram(m_program);
	}
}

void ShaderProgram::CreateProgram(bool bHasGeometryShader, bool bHasTessControlShader, bool bHasComputeShader)
{
	m_program = gl::CreateProgram();

	// Configure the pipelne
	std::vector<ShaderStage> pipeline;

	// Avoid calling the destructor prematurely on each move
	// At most 6 stages.
	pipeline.reserve(6);

	// Mandatory stages
	pipeline.emplace_back(m_name, ShaderType::VERTEX, m_program);
	pipeline.emplace_back(m_name, ShaderType::FRAGMENT, m_program);

	// Optional stages
	if (bHasGeometryShader)
	{
		pipeline.emplace_back(m_name, ShaderType::GEOMERTRY, m_program);
	}

	if (bHasComputeShader)
	{
		pipeline.emplace_back(m_name, ShaderType::COMPUTE, m_program);
	}

	if (bHasTessControlShader)
	{
		pipeline.emplace_back(m_name, ShaderType::TESS_CONTROL, m_program);
		pipeline.emplace_back(m_name, ShaderType::TESS_EVAL, m_program);
	}

	// Attach shaders and link program
	for (const auto& stage : pipeline)
	{
		gl::AttachShader(m_program, stage.GetShaderObject());
	}

	gl::LinkProgram(m_program);

	GLint status = 0;
	gl::GetProgramiv(m_program, gl::LINK_STATUS, &status);
	if (status == 0)
	{
		char buffer[512];
		gl::GetProgramInfoLog(m_program, 512, nullptr, buffer);
		throw ShaderLinkingError(buffer);
	}
}