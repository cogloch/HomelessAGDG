#pragma once
#include "Vertex.h"
#include "GLCore.h"


class Quad
{
public:
	void Create();
	void Draw();

private:
	U32 VAO, VBO;
};