#include "PCH.h"
#include "RenderItem.h"
#include "ShaderGuard.h"
#include "ResourceCache.h"
#include "Projection.h"
#include "Camera.h"


RenderItem::RenderItem(ResourceHandle shader, ResourceHandle mesh)
	: m_mesh(mesh)
	, m_shader(shader)
{
}

void RenderItem::Render(const glm::mat4& viewProjMat) const
{
	ShaderGuard shader(g_resourceCache.GetShader(m_shader));

	shader.SetUniform("model", m_transform);
	shader.SetUniform("viewProj", viewProjMat);

	g_resourceCache.GetMesh(m_mesh).Draw();
}

void RenderItem::SetTransform(const glm::mat4& newTransformMat)
{
	m_transform = newTransformMat;
}