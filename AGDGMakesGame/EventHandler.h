#pragma once
#include "Event.h"


class EventHandler 
{
public:
	virtual ~EventHandler();
	virtual void Handle(const Event&) = 0;

protected:
	EventHandler();
};