#include "PCH.h"
#include "Vertex.h"


void Vertex2D::LinkVertexAttrib()
{
	// Position
	gl::EnableVertexAttribArray(0);
	gl::VertexAttribPointer(0, 2, gl::FLOAT, 0, sizeof(Vertex2D), (void*)(0));
}

void Vertex2DTexture::LinkVertexAttrib()
{
	// Position
	gl::EnableVertexAttribArray(0);
	gl::VertexAttribPointer(0, 2, gl::FLOAT, 0, sizeof(Vertex2DTexture), (void*)(0));

	// Texture Coordinates
	gl::EnableVertexAttribArray(1);
	gl::VertexAttribPointer(1, 2, gl::FLOAT, 0, sizeof(Vertex2DTexture), (void*)offsetof(Vertex2DTexture, texCoord));
}

Vertex3D::Vertex3D(float x, float y, float z)
{
	position[0] = x;
	position[1] = y;
	position[2] = z;
}

void Vertex3D::LinkVertexAttrib()
{
	// Position
	gl::EnableVertexAttribArray(0);
	gl::VertexAttribPointer(0, 3, gl::FLOAT, 0, sizeof(Vertex3D), (void*)(0));
}

void Vertex::LinkVertexAttrib()
{
	// Position
	gl::EnableVertexAttribArray(0);
	gl::VertexAttribPointer(0, 3, gl::FLOAT, 0, sizeof(Vertex), (void*)(0));

	// Normal
	gl::EnableVertexAttribArray(1);
	gl::VertexAttribPointer(1, 3, gl::FLOAT, 0, sizeof(Vertex), (void*)offsetof(Vertex, normal));

	// Texture Coords
	gl::EnableVertexAttribArray(2);
	gl::VertexAttribPointer(2, 2, gl::FLOAT, 0, sizeof(Vertex), (void*)offsetof(Vertex, texCoord));
}