#pragma once
#include "Timer.h"
#include "Window.h"
#include "Renderer.h"
#include "GLContext.h"
#include "UIManager.h"
#include "TransformNoScale.h" // TEMP
#include "CameraController.h"


class Game
{
public:
	Game(HINSTANCE);
	~Game();

	// Handles init/shutdown and the main loop.
	// Returns the exit status.
	U32 Run();

private:
	static bool s_bInstantiated;
	Timer     m_frameTimer;
	Window    m_window;
	Renderer  m_renderer;
	UIManager m_uiManager;
	GLContext m_context;
	HINSTANCE m_hInstance;
	CameraController m_camController;
	TransformNoScale m_camTransform;

private:
	// Returns fatal error code or 0 if successful.
	U32 InitSystems();

	// Returns fatal error code or 0 if successful.
	U32 CloseSystems(); // TODO: Find a better name.

	// Update game state. Not sure if we should do updating this way or let updateable objects register for a "Frame" or "Update" or whatever event.
	// To discuss.
	void Update(float dt);
};