#include "PCH.h"
#include "TransformNoScale.h"


TransformNoScale::TransformNoScale()
	: m_position(0.0f, 0.0f, 0.0f)
	, m_rotation(1.0f, 0.0f, 0.0f, 0.0f)
	, m_transformMatrix(1.0f)
{
	CacheTransform();
}

TransformNoScale::TransformNoScale(const glm::vec3& position)
	: m_position(position)
	, m_rotation(1.0f, 0.0f, 0.0f, 0.0f)
	, m_transformMatrix(1.0f)
{
	CacheTransform();
}

TransformNoScale::TransformNoScale(const glm::vec3& position, const glm::quat& rotation)
	: m_position(position)
	, m_rotation(rotation)
	, m_transformMatrix(1.0f)
{
	CacheTransform();
}

TransformNoScale::TransformNoScale(const glm::vec3& position, float pitch, float yaw, float roll)
	: m_position(position)
	, m_rotation(glm::vec3(pitch, yaw, roll))
	, m_transformMatrix(1.0f)
{
	CacheTransform();
}

bool operator==(const TransformNoScale& lhs, const TransformNoScale& rhs)
{
	return lhs.m_transformMatrix == rhs.m_transformMatrix;
}

bool operator!=(const TransformNoScale& lhs, const TransformNoScale& rhs)
{
	return lhs.m_transformMatrix != rhs.m_transformMatrix;
}

void TransformNoScale::SetPosition(const glm::vec3& newPosition)
{
	m_position = newPosition;
	CacheTransform();
}

void TransformNoScale::SetPosition(float x, float y, float z)
{
	m_position = { x, y, z };
	CacheTransform();
}

void TransformNoScale::SetRotation(const glm::mat4& rotMatrix)
{
	m_rotation = glm::quat_cast(rotMatrix);
	CacheTransform();
}

void TransformNoScale::SetRotation(const glm::vec3& newRotation)
{
	m_rotation = glm::quat(newRotation);
	CacheTransform();
}

void TransformNoScale::SetRotation(float pitchAngle, float yawAngle, float rollAngle)
{
	m_rotation = glm::quat(glm::vec3(pitchAngle, yawAngle, rollAngle));
	CacheTransform();
}

void TransformNoScale::SetRotation(const glm::quat& rotation)
{
	m_rotation = rotation;
	CacheTransform();
}

void TransformNoScale::Translate(const glm::vec3& dPosition)
{
	m_position += dPosition;
	CacheTransform();
}

void TransformNoScale::Translate(float dX, float dY, float dZ)
{
	m_position += glm::vec3(dX, dY, dZ);
	CacheTransform();
}

void TransformNoScale::TranslateX(float dX)
{
	m_position.x += dX;
	CacheTransform();
}

void TransformNoScale::TranslateY(float dY)
{
	m_position.y += dY;
	CacheTransform();
}

void TransformNoScale::TranslateZ(float dZ)
{
	m_position.z += dZ;
	CacheTransform();
}

void TransformNoScale::Rotate(const glm::quat& other)
{
	m_rotation = other * m_rotation;
	CacheTransform();
}

void TransformNoScale::RotateX(float dPitchAngle)
{
	m_rotation = glm::angleAxis(dPitchAngle, xAxis) * m_rotation;
	CacheTransform();
}

void TransformNoScale::RotateY(float dYawAngle)
{
	m_rotation = glm::angleAxis(dYawAngle, yAxis) * m_rotation;
	CacheTransform();
}

void TransformNoScale::RotateZ(float dRollAngle)
{
	m_rotation = glm::angleAxis(dRollAngle, zAxis) * m_rotation;
	CacheTransform();
}

void TransformNoScale::CacheTransform()
{
	// Translate -> Rotate
	m_transformMatrix = glm::translate(glm::mat4(1.0f), m_position) * glm::toMat4(m_rotation);
}