#pragma once

// Change to 0 if redistributing 
#define DEV 1

#include "Types.h"

// Windows Headers
#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <windowsx.h>
#endif

// C++ Headers
#include <algorithm>
#include <stdexcept>
#include <cassert>
#include <fstream>
#include <vector>
#include <string>
#include <memory>
#include <list>
#include <map>

// OpenGL Headers
#include "GLCore.h"
#include <glm\glm.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtc\quaternion.hpp>
#include <glm\gtx\quaternion.hpp>
#include <glm\gtc\matrix_transform.hpp>