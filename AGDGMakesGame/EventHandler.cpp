#include "PCH.h"
#include "EventHandler.h"
#include "EventBroadcaster.h"


EventHandler::EventHandler()
{
	g_eventBroadcaster.AddHandler(this);
}

EventHandler::~EventHandler()
{
	g_eventBroadcaster.RemoveHandler(this);
}