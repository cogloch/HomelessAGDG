#include "PCH.h"
#include "Mesh.h"
#include "ResourceCache.h"
#include "tiny_obj_loader.h"


Mesh::Mesh()
	: VAO(0)
	, VBO(0)
	, EBO(0)
{
}

void Mesh::Draw() const
{
	gl::BindVertexArray(VAO);
	gl::DrawElements(gl::TRIANGLES, (GLsizei)indices.size(), gl::UNSIGNED_INT, 0);
	gl::BindVertexArray(0);
}

void Mesh::Init(const std::vector<Vertex>& vertices_, const std::vector<U32>& indices_)
{
	// Consider not storing vert/index data here(current idea is that they may be updated)
	vertices = vertices_;
	indices = indices_;

	gl::GenVertexArrays(1, &VAO);
	gl::GenBuffers(1, &VBO);
	gl::GenBuffers(1, &EBO);

	gl::BindVertexArray(VAO);

	gl::BindBuffer(gl::ARRAY_BUFFER, VBO);
	gl::BufferData(gl::ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], gl::STATIC_DRAW);

	gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, EBO);
	gl::BufferData(gl::ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(U32), &indices[0], gl::STATIC_DRAW);

	Vertex::LinkVertexAttrib();

	gl::BindVertexArray(0);
}

void Mesh::Init(const std::string& filename)
{
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;

	std::string path = g_resourceCache.GetMeshesPath() + filename;

	std::string err = tinyobj::LoadObj(shapes, materials, path.c_str());
	if (!err.empty())
	{
		throw std::runtime_error(err);
	}

	for (auto& shape : shapes)
	{
		for (auto& idx : shape.mesh.indices)
		{
			indices.push_back(idx);
		}

		vertices.resize(shape.mesh.positions.size() / 3);

		for (auto vert = 0; vert < shape.mesh.positions.size() / 3; vert++)
		{
			vertices[vert].position[0] = shape.mesh.positions[vert * 3];
			vertices[vert].position[1] = shape.mesh.positions[vert * 3 + 1];
			vertices[vert].position[2] = shape.mesh.positions[vert * 3 + 2];
		}

		for (auto vert = 0; vert < shape.mesh.normals.size() / 3; vert++)
		{
			vertices[vert].normal[0] = shape.mesh.normals[vert * 3];
			vertices[vert].normal[1] = shape.mesh.normals[vert * 3 + 1];
			vertices[vert].normal[2] = shape.mesh.normals[vert * 3 + 2];
		}

		for (auto vert = 0; vert < shape.mesh.texcoords.size() / 2; vert++)
		{
			vertices[vert].texCoord[0] = shape.mesh.texcoords[vert * 3];
			vertices[vert].texCoord[1] = shape.mesh.texcoords[vert * 3 + 1];
		}
	}

	Init(vertices, indices);
}