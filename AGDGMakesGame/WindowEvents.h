#pragma once
#include "Event.h"
#include <utility>


Event&& CreateWindowResizeEvent(U32 width, U32 height)
{
	Event ev;
	ev.type = EventType::WINDOW_RESIZE;
	ev.length = 2;
	ev.data[0] = EventData(width);
	ev.data[1] = EventData(height);

	return std::move(ev);
}