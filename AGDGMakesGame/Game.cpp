#include "PCH.h"
#include "Game.h"
#include "InputManager.h"
#include "ResourceCache.h"
#include "RenderComponent.h"
#include "GameStateEvents.h"
#include "EventBroadcaster.h"


bool Game::s_bInstantiated = false;

EventBroadcaster g_eventBroadcaster;
ResourceCache    g_resourceCache;
InputManager     g_inputManager;

Game::Game(HINSTANCE hInstance_)
	: m_hInstance(hInstance_)
{
	assert(!s_bInstantiated);
	s_bInstantiated = true;
}

Game::~Game()
{
	s_bInstantiated = false;
}
std::vector<RenderComponent> comps; // TODO: Entity system


U32 Game::InitSystems()
{
	try
	{
		m_window.Init(m_hInstance, 800, 600, L"Homeless AGDG");
		m_context.Init(m_window.GetHandle(), 800, 600, 4, 3);
		m_renderer.Init(&m_context);
		g_resourceCache.Init();
		m_uiManager.Init(&m_renderer, g_resourceCache.GetUILayoutsPath());
		m_camController.Attach(&m_camTransform);

		// TEMP
		m_uiManager.SetActiveLayout(0);
		comps.push_back(RenderComponent(&m_renderer, "Test", "test.obj"));
	}
	catch (std::runtime_error& err)
	{
		MessageBoxA(m_window.GetHandle(), err.what(), "Initialization Error", MB_OK);
		return 1;
	}

	return 0;
}

U32 Game::CloseSystems()
{
	return 0;
}

void Game::Update(float dt)
{
}

U32 Game::Run()
{
	U32 errorCode = InitSystems();
	if (errorCode)
	{
		return errorCode;
	}

	m_frameTimer.Reset();

	while (!m_window.ShouldClose())
	{
		m_window.RunMessageLoop();
		
		auto dt = m_frameTimer.Update();
		g_eventBroadcaster.AddEvent(CreateTickEvent(dt));
		g_eventBroadcaster.Broadcast(dt);

		Update(dt);
		m_renderer.UpdateView(m_camTransform);
		m_renderer.UpdateScene(comps); // TEMP: Send all render components to be rendered 
								       //       Will go instead through a filter(big world, top down camera, insanely wasteful not to)
		
		m_renderer.UpdateUI(m_uiManager.GetActiveLayout().uiElements);
		m_renderer.Render();
	}

	errorCode = CloseSystems();
	return errorCode;
}