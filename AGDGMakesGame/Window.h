#pragma once


class Window
{
public:
	Window();
	~Window();

	void Init(HINSTANCE, U32 width = 800, U32 height = 600, const std::wstring& caption = L"Window", bool bBorderless = false);

	// Always call ShouldClose() after running the message loop.
	void RunMessageLoop();
	const bool ShouldClose() const;

	const HWND GetHandle() const;
	const U32  GetWidth() const;
	const U32  GetHeight() const;

private:
	HWND m_hWnd;
	DWORD m_windowStyle;
	U32 m_positionX;
	U32 m_positionY;
	U32 m_width;
	U32 m_height;
	bool m_bShouldClose;

private:
	static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
	static std::map<HWND, Window*> s_windows;
};

inline const bool Window::ShouldClose() const
{
	return m_bShouldClose;
}

inline const HWND Window::GetHandle() const
{
	return m_hWnd;
}

inline const U32  Window::GetWidth() const
{
	return m_width;
}

inline const U32 Window::GetHeight() const
{
	return m_height;
}