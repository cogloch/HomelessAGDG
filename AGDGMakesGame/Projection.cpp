#include "PCH.h"
#include "Projection.h"


Projection::Projection(float viewWidth, float viewHeight, float nearPlane, float farPlane)
	: m_viewWidth(viewWidth), m_viewHeight(viewHeight), m_nearPlane(nearPlane), m_farPlane(farPlane)
{
}

OrthoProjection::OrthoProjection(float viewWidth, float viewHeight, float nearPlane, float farPlane)
	: Projection(viewWidth, viewHeight, nearPlane, farPlane)
{
	CacheMatrix();
}

void OrthoProjection::CacheMatrix()
{
	m_matrix = glm::ortho(0.0f, m_viewWidth, m_viewHeight, 0.0f, m_nearPlane, m_farPlane);
}

PerspectiveProjection::PerspectiveProjection(float viewWidth, float viewHeight, float fovY, float nearPlane, float farPlane)
	: Projection(viewWidth, viewHeight, nearPlane, farPlane)
	, m_fovY(fovY)
{
	CacheMatrix();
}

void PerspectiveProjection::CacheMatrix()
{
	m_matrix = glm::perspective(m_fovY, m_viewWidth / m_viewHeight, m_nearPlane, m_farPlane);
}