#include "PCH.h"
#include "InputManager.h"
#include "CameraController.h"


CameraController::CameraController()
{
}

void CameraController::Attach(TransformNoScale* pCamTransform)
{
	m_pCamTransform = pCamTransform;
	Init();
}

void CameraController::Update(float dt)
{
	//////////////////////////////////////////////
	// TODO: Separate 
	auto rotMat = glm::toMat4(m_pCamTransform->GetRotationQuat());
	glm::vec3 upVec   (rotMat[0][1], rotMat[1][1], rotMat[2][1]), 
			  rightVec(rotMat[0][0], rotMat[1][0], rotMat[2][0]), 
			  fwdVec  (rotMat[0][2], rotMat[1][2], rotMat[2][2]);
	//////////////////////////////////////////////

	float lDistance = 10.0f * dt;
	float rDistance = 1.0f * dt;
	auto& rotationQuat = m_pCamTransform->GetRotationQuat();

	if (g_inputManager.IsKeyPressed('W'))
		m_pCamTransform->Translate(fwdVec * -lDistance);

	if (g_inputManager.IsKeyPressed('S'))
		m_pCamTransform->Translate(fwdVec * lDistance);

	if (g_inputManager.IsKeyPressed('D'))
		m_pCamTransform->Translate(rightVec * lDistance);

	if (g_inputManager.IsKeyPressed('A'))
		m_pCamTransform->Translate(rightVec * -lDistance);

	if (g_inputManager.IsKeyPressed('T'))
		m_pCamTransform->Translate(upVec * lDistance * 0.5f);

	if (g_inputManager.IsKeyPressed('G'))
		m_pCamTransform->Translate(upVec * -lDistance * 0.5f);

	if (g_inputManager.IsKeyPressed('I'))
		m_pCamTransform->RotateX(rDistance);

	if (g_inputManager.IsKeyPressed('K'))
		m_pCamTransform->RotateX(-rDistance);

	if (g_inputManager.IsKeyPressed('J'))
		m_pCamTransform->RotateY(rDistance);

	if (g_inputManager.IsKeyPressed('L'))
		m_pCamTransform->RotateY(-rDistance);

	if (g_inputManager.IsKeyPressed('Q'))
		m_pCamTransform->RotateZ(-rDistance);

	if (g_inputManager.IsKeyPressed('E'))
		m_pCamTransform->RotateZ(rDistance);
}

void CameraController::Init()
{
	glm::vec3 focalPoint(0.0f, 0.0f, 0.0f);
	glm::vec3 up(0.0f, 1.0f, 0.0f);
	glm::vec3 pos(0.0f, 0.0f, -3.0f);
	
	m_pCamTransform->SetPosition(pos);

	auto fwd = glm::normalize(focalPoint - pos);
	auto right = glm::normalize(glm::cross(up, fwd));
	auto upVec = glm::normalize(glm::cross(fwd, right));

	SetAxis(fwd, upVec, right);
}

void CameraController::SetAxis(const glm::vec3& fwd, const glm::vec3& up, const glm::vec3& right)
{
	glm::mat4 rotMat(1.0f);

	// right.x | up.x | fwd.x |  0
	// right.y | up.y | fwd.y |  0
	// right.z | up.z | fwd.z |  0
	//    0    |  0   |   0   |  1

	rotMat[0][0] = right.x;
	rotMat[0][1] = up.x;
	rotMat[0][2] = fwd.x;

	rotMat[1][0] = right.y;
	rotMat[1][1] = up.y;
	rotMat[1][2] = fwd.y;

	rotMat[2][0] = right.z;
	rotMat[2][1] = up.z;
	rotMat[2][2] = fwd.z;

	m_pCamTransform->SetRotation(rotMat);
}

void CameraController::Handle(const Event& ev)
{
	switch (ev.type)
	{
	case EventType::TICK:
		Update(ev.data[0].asFloat);
		break;
	}
}