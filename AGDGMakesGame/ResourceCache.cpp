#include "PCH.h"
#include "ResourceCache.h"
#include "SOIL\SOIL.h"


bool ResourceCache::s_bInstantiated = false;

ResourceCache::ResourceCache()
{
	SolveAbsolutePaths();
	s_bInstantiated = true;
}

void ResourceCache::Init()
{
	m_quad.Create();

	g_resourceCache.LoadAllMeshes();
	g_resourceCache.LoadAllShaders();
	g_resourceCache.LoadAllTextures();
}

void ResourceCache::LoadAllShaders()
{
	std::ifstream descriptorFile(m_shadersPath + "ShaderList.txt");

	std::string line;
	std::getline(descriptorFile, line); // Comment

	while (std::getline(descriptorFile, line))
	{
		LoadShader(line);
	}
}

ResourceHandle ResourceCache::LoadShader(const std::string& name)
{
	// TODO: Scan shader folder for all files with @name to determine optional stages presence
	//       either here on in the ShaderProgram code
	m_shaders.push_back(ShaderProgram(name)); // TODO: Pass full path to remove dependency the other way around
	
	ResourceHandle newResHandle = m_shaders.size() - 1;
	m_shaderNames[name] = newResHandle;
	return newResHandle;
}

void ResourceCache::LoadAllMeshes()
{
	std::ifstream descriptorFile(m_meshesPath + "MeshList.txt");
	
	std::string line;
	std::getline(descriptorFile, line); // Comment

	while (std::getline(descriptorFile, line))
	{
		LoadMesh(line);
	}
}

ResourceHandle ResourceCache::LoadMesh(const std::string& name)
{
	// TODO: Loading in constructor
	Mesh mesh;
	mesh.Init(name); // TODO: Pass full path to remove dependency the other way around
	m_meshes.push_back(mesh);

	ResourceHandle newResHandle = m_meshes.size() - 1;
	m_meshNames[name] = newResHandle;
	return newResHandle;
}

void ResourceCache::LoadAllTextures()
{
	std::ifstream descriptorFile(m_texturesPath + "TextureList.txt");

	std::string line;
	std::getline(descriptorFile, line); // Comment

	while (std::getline(descriptorFile, line))
	{
		LoadTexture(line);
	}
}

ResourceHandle ResourceCache::LoadTexture(const std::string& name)
{
	I32 width = 0;
	I32 height = 0;
	auto image = SOIL_load_image((m_texturesPath + name).c_str(), &width, &height, 0, SOIL_LOAD_RGB);
	
	// TODO: Loading in constructor
	Texture texture;
	texture.Init(width, height, image);
	m_textures.push_back(texture);
	
	SOIL_free_image_data(image);

	ResourceHandle newResHandle = m_textures.size() - 1;
	m_textureNames[name] = newResHandle;
	return newResHandle;
}

void ResourceCache::SolveAbsolutePaths()
{
	char path[MAX_PATH];
	GetModuleFileNameA(NULL, path, MAX_PATH);
	std::string m_assetsPath = path;

#if DEV
	// Root is the Solution folder

	// Remove executable name and trailing slash
	m_assetsPath = m_assetsPath.substr(0, m_assetsPath.find_last_of("\\"));

	// Remove build configuration folder and trailing slash
	m_assetsPath = m_assetsPath.substr(0, m_assetsPath.find_last_of("\\"));

	// Remove Bin folder
	m_assetsPath = m_assetsPath.substr(0, m_assetsPath.find_last_of("\\") + 1);
#else /* REDISTRIBUTING */
	// Root is the executable folder

	// Remove executable name and trailing slash
	m_assetsPath = m_assetsPath.substr(0, m_assetsPath.find_last_of("\\") + 1);
#endif

	m_assetsPath += "Assets\\";

	m_shadersPath = m_assetsPath + "Shaders\\";
	m_meshesPath = m_assetsPath + "Meshes\\";
	m_texturesPath = m_assetsPath + "Textures\\";
	m_uiLayoutsPath = m_assetsPath + "UILayouts\\";
}
