#include "PCH.h"
#include "RenderItemUI.h"
#include "ShaderGuard.h"
#include "ResourceCache.h"


RenderItemUI::RenderItemUI(ResourceHandle texture)
{
	m_texture = texture;
}

void RenderItemUI::Render(const glm::mat4& projMat) const
{
	ShaderGuard shader(g_resourceCache.GetShader("Sprite"));

	shader.SetUniform("model", m_transform);
	shader.SetUniform("proj", projMat);

	gl::ActiveTexture(gl::TEXTURE0);
	gl::BindTexture(gl::TEXTURE_2D, g_resourceCache.GetTexture(m_texture).texture);
	g_resourceCache.GetQuad().Draw();
}

void RenderItemUI::SetTransform(const glm::mat4& transform)
{
	m_transform = transform;
}