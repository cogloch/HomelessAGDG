#pragma once
#include "EventHandler.h"
#include "TransformNoScale.h"
#include "PCH.h"


class CameraController : public EventHandler
{
public:
	CameraController();
	void Attach(TransformNoScale* pCamTransform);
	void Update(float dt);
	void Handle(const Event& ev);

private:
	void Init();
	void SetAxis(const glm::vec3& fwd, const glm::vec3& up, const glm::vec3& right);

	TransformNoScale* m_pCamTransform;
};