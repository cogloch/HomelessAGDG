#include "PCH.h"
#include "ShaderGuard.h"


ShaderGuard::ShaderGuard(const ShaderProgram& shaderProgram)
{
	m_oglName = shaderProgram.GetProgram();
	
	AssertNoShaderBound();
	gl::UseProgram(m_oglName);
}

ShaderGuard::ShaderGuard(const std::string& shaderFilename)
{
	// TODO: Get cached shader
	//m_oglName = g_resources.GetShaderProgramByName(shaderFilename);

	AssertNoShaderBound();
	gl::UseProgram(m_oglName);
}

ShaderGuard::ShaderGuard(const U32 oglName)
{
	m_oglName = oglName;

	AssertNoShaderBound();

	if (!gl::IsProgram(oglName))
	{
		throw ShaderBindingError("Attempting to bind a shader program with an invalid name.");
	}

	gl::UseProgram(m_oglName);
}

void ShaderGuard::AssertNoShaderBound()
{
	I32 boundObject = 0;
	gl::GetIntegerv(gl::CURRENT_PROGRAM, &boundObject);

	if (boundObject)
	{
		//auto boundName = g_resources.GetShaderProgramName(boundObject);
		//auto currentName = g_resources.GetShaderProgramName(m_oglName);
		std::string boundName = "Temp - No res cache";
		std::string currentName = "Temp - No res cache";
		throw ShaderBindingError("Attempting to bind " + currentName + " while there was already a bound program: " + boundName);
	}
}

ShaderGuard::~ShaderGuard()
{
	I32 boundObject = 0;
	gl::GetIntegerv(gl::CURRENT_PROGRAM, &boundObject);
	if (boundObject == m_oglName)
	{
		gl::UseProgram(0);
	}
}

I32 ShaderGuard::GetUniformLocation(const std::string& uniformName) const
{
	I32 loc = gl::GetUniformLocation(m_oglName, uniformName.c_str());

	if (loc == -1)
	{
		throw ShaderBindingError("Bound shader does not have the uniform \"" + uniformName + "\".");
	}

	return loc;
}

void ShaderGuard::SetUniform(const std::string& name, float value0) const
{
	gl::Uniform1f(GetUniformLocation(name), value0);
}

void ShaderGuard::SetUniform(const std::string& name, float value0, float value1) const
{
	gl::Uniform2f(GetUniformLocation(name), value0, value1);
}

void ShaderGuard::SetUniform(const std::string& name, float value0, float value1, float value2) const
{
	gl::Uniform3f(GetUniformLocation(name), value0, value1, value2);
}

void ShaderGuard::SetUniform(const std::string& name, float value0, float value1, float value2, float value3) const
{
	gl::Uniform4f(GetUniformLocation(name), value0, value1, value2, value3);
}

void ShaderGuard::SetUniform(const std::string& name, I32 value0) const
{
	gl::Uniform1i(GetUniformLocation(name), value0);
}

void ShaderGuard::SetUniform(const std::string& name, I32 value0, I32 value1) const
{
	gl::Uniform2i(GetUniformLocation(name), value0, value1);
}

void ShaderGuard::SetUniform(const std::string& name, I32 value0, I32 value1, I32 value2) const
{
	gl::Uniform3i(GetUniformLocation(name), value0, value1, value2);
}

void ShaderGuard::SetUniform(const std::string& name, I32 value0, I32 value1, I32 value2, I32 value3) const
{
	gl::Uniform4i(GetUniformLocation(name), value0, value1, value2, value3);
}

void ShaderGuard::SetUniform(const std::string& name, const glm::vec2& value) const
{
	gl::Uniform2fv(GetUniformLocation(name), 1, glm::value_ptr(value));
}

void ShaderGuard::SetUniform(const std::string& name, const glm::vec3& value) const
{
	gl::Uniform3fv(GetUniformLocation(name), 1, glm::value_ptr(value));
}

void ShaderGuard::SetUniform(const std::string& name, const glm::vec4& value) const
{
	gl::Uniform4fv(GetUniformLocation(name), 1, glm::value_ptr(value));
}

void ShaderGuard::SetUniform(const std::string& name, const glm::mat2& value, GLboolean transpose) const
{
	gl::UniformMatrix2fv(GetUniformLocation(name), 1, transpose, glm::value_ptr(value));
}

void ShaderGuard::SetUniform(const std::string& name, const glm::mat3& value, GLboolean transpose) const
{
	gl::UniformMatrix3fv(GetUniformLocation(name), 1, transpose, glm::value_ptr(value));
}

void ShaderGuard::SetUniform(const std::string& name, const glm::mat4& value, GLboolean transpose) const
{
	gl::UniformMatrix4fv(GetUniformLocation(name), 1, transpose, glm::value_ptr(value));
}

void ShaderGuard::SetUniform(I32 loc, float value0) const
{
	gl::Uniform1f(loc, value0);
}

void ShaderGuard::SetUniform(I32 loc, float value0, float value1) const
{
	gl::Uniform2f(loc, value0, value1);
}

void ShaderGuard::SetUniform(I32 loc, float value0, float value1, float value2) const
{
	gl::Uniform3f(loc, value0, value1, value2);
}

void ShaderGuard::SetUniform(I32 loc, float value0, float value1, float value2, float value3) const
{
	gl::Uniform4f(loc, value0, value1, value2, value3);
}

void ShaderGuard::SetUniform(I32 loc, I32 value0) const
{
	gl::Uniform1i(loc, value0);
}

void ShaderGuard::SetUniform(I32 loc, I32 value0, I32 value1) const
{
	gl::Uniform2i(loc, value0, value1);
}

void ShaderGuard::SetUniform(I32 loc, I32 value0, I32 value1, I32 value2) const
{
	gl::Uniform3i(loc, value0, value1, value2);
}

void ShaderGuard::SetUniform(I32 loc, I32 value0, I32 value1, I32 value2, I32 value3) const
{
	gl::Uniform4i(loc, value0, value1, value2, value3);
}

void ShaderGuard::SetUniform(I32 loc, const glm::vec2& value) const
{
	gl::Uniform2fv(loc, 1, glm::value_ptr(value));
}

void ShaderGuard::SetUniform(I32 loc, const glm::vec3& value) const
{
	gl::Uniform3fv(loc, 1, glm::value_ptr(value));
}

void ShaderGuard::SetUniform(I32 loc, const glm::vec4& value) const
{
	gl::Uniform4fv(loc, 1, glm::value_ptr(value));
}

void ShaderGuard::SetUniform(I32 loc, const glm::mat2& value, GLboolean transpose) const
{
	gl::UniformMatrix2fv(loc, 1, transpose, glm::value_ptr(value));
}

void ShaderGuard::SetUniform(I32 loc, const glm::mat3& value, GLboolean transpose) const
{
	gl::UniformMatrix3fv(loc, 1, transpose, glm::value_ptr(value));
}

void ShaderGuard::SetUniform(I32 loc, const glm::mat4& value, GLboolean transpose) const
{
	gl::UniformMatrix4fv(loc, 1, transpose, glm::value_ptr(value));
}