#pragma once
#include "Types.h"


enum class DataType : U32
{
	U8,
	U16,
	U32,
	BOOL,
	FLOAT
};

struct EventData 
{
	EventData() {}
	explicit EventData(U8 data)    : type(DataType::U8)    { asU8 = data; }
	explicit EventData(U16 data)   : type(DataType::U16)   { asU16 = data; }
	explicit EventData(U32 data)   : type(DataType::U32)   { asU32 = data; }
	explicit EventData(bool data)  : type(DataType::BOOL)  { asBool = data; }
	explicit EventData(float data) : type(DataType::FLOAT) { asFloat = data; }

	DataType type; // 4 bytes
	union
	{
		U8    asU8;
		U16   asU16;
		U32   asU32;
		bool  asBool;
		float asFloat;
	}; // 4 bytes
};

// Maximum amount of 'data', or arguments, an event may have.
const U32 MAX_EVENT_DATA = 7;	

enum class EventType : U16
{ 
	UNKNOWN,

	// Mouse Input
	LMOUSE_UP,
	LMOUSE_DOWN,
	RMOUSE_UP,
	RMOUSE_DOWN,
	MOUSE_MOVE,
	MOUSE_WHEEL,

	// Keyboard Input
	KEY_UP,
	KEY_DOWN,
	KEY_CHAR,

	WINDOW_RESIZE,
	TICK,

	NUM_EVENT_TYPES
};

// type   - 2 bytes
// length - 2 bytes
// delay  - 4 bytes
// data   - 56 bytes
// Total  - 64 bytes / Event 
struct Event
{
	EventType type;
	U16       length;
	// Number of seconds to delay the dispatching of this event by.
	float     delay;
	EventData data[MAX_EVENT_DATA];

	Event();
};

inline Event::Event() : type(EventType::UNKNOWN), length(0), delay(0.0f) { }
