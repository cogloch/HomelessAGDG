#pragma once
#include "GLCore.h"


struct Vertex2D
{
	float position[2];
	static void LinkVertexAttrib();
};

struct Vertex2DTexture
{
	float position[2];
	float texCoord[2];
	static void LinkVertexAttrib();
};

struct Vertex3D
{
	float position[3];
	Vertex3D(float x, float y, float z);
	static void LinkVertexAttrib();
};

struct Vertex3DTexture
{
	float position[3];
	float texCoord[2];

	// TODO: Vertex attrib linking
};

struct Vertex
{
	float position[3];
	float normal[3];
	float texCoord[2];
	static void LinkVertexAttrib();
};