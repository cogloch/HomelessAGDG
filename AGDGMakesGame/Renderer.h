#pragma once
#include "Camera.h"
#include "GLContext.h"
#include "UIElement.h"
#include "RenderItem.h"
#include "Projection.h"
#include "RenderItemUI.h"
#include "RenderComponent.h"


class Renderer
{
public:
	Renderer();
	void Init(GLContext*);
	void Render();

	void UpdateUI(const std::vector<UIElement>&);
	void UpdateView(const TransformNoScale& camTransform);
	void UpdateScene(const std::vector<RenderComponent>&);

	// Returns the id of the newly created render item.
	U32 AddRenderItem(ResourceHandle shader, ResourceHandle mesh);
	U32 AddUIRenderItem(ResourceHandle texture);

private:
	GLContext* m_pContext;

	static const size_t MAX_RENDER_QUEUE_ITEMS = 100;

	// Holds ids of render items that are going to be rendered this frame.
	U32 m_renderQueue[MAX_RENDER_QUEUE_ITEMS];
	U32 m_renderQueueEnd;
	U32 m_renderQueueUI[MAX_RENDER_QUEUE_ITEMS];
	U32 m_renderQueueUIEnd;

	// Holds all render items, whether they are to be rendered or not.
	std::vector<RenderItem> m_renderItems;
	std::vector<RenderItemUI> m_renderItemsUI;

	Camera m_camera;
	PerspectiveProjection m_projPerspective;
	OrthoProjection m_projOrthographic;

private:
	struct RenderItemCacheRequest
	{
		ResourceHandle shader;
		ResourceHandle mesh;
	};
	std::vector<RenderItemCacheRequest> m_renderItemCacheRequets;

	struct UIRenderItemCacheRequest
	{
		ResourceHandle texture;
	};
	std::vector<UIRenderItemCacheRequest> m_uiRenderItemCacheRequets;
};
