#include "PCH.h"
#include "UIElement.h"
#include "Renderer.h"
#include "ResourceCache.h"


// Account for (cross)(title bar)(/cross) mysterious offset thingamagig 
float UIElement::s_offsetY = 39.0f;

UIElement::UIElement(Renderer* pRenderer, const std::string& textureName)
{auto& tex = g_resourceCache.GetTexture(textureName);
	m_width = tex.width;
	m_height = tex.height;
	SetSize(m_width, m_height);
	m_renderItemId = pRenderer->AddUIRenderItem(g_resourceCache.GetTextureHandle(textureName));
}

void UIElement::SetPosition(const glm::vec2& position)
{
	m_transform.SetPosition(position.x, position.y + s_offsetY, 0.0f);
}

void UIElement::SetPosition(float x, float y)
{
	m_transform.SetPosition(x, y + s_offsetY, 0.0f);
}

void UIElement::SetRotation(float angleInDeg)
{
	m_transform.SetRotation(0.0f, 0.0f, angleInDeg);
}

void UIElement::SetSize(float uniformScale)
{
	m_transform.SetScale(uniformScale, uniformScale, 1.0f);
}

void UIElement::SetSize(const glm::vec2& scale)
{
	m_transform.SetScale(glm::vec3(scale, 1.0f));
}

void UIElement::SetSize(float scaleX, float scaleY)
{
	m_transform.SetScale(scaleX, scaleY, 1.0f);
}

void UIElement::Translate(const glm::vec2& dPosition)
{
	m_transform.Translate(glm::vec3(dPosition, 0.0f));
}

void UIElement::Translate(float x, float y)
{
	m_transform.Translate(x, y, 0.0f);
}

void UIElement::Rotate(float angleInDeg)
{
	m_transform.RotateZ(angleInDeg);
}

void UIElement::Scale(float uniformScale)
{
	m_transform.Scale(uniformScale, uniformScale, 1.0f);
}

void UIElement::Scale(const glm::vec2& dScale)
{
	m_transform.Scale(glm::vec3(dScale, 1.0f));
}

void UIElement::Scale(float scaleX, float scaleY)
{
	m_transform.Scale(scaleX, scaleY, 1.0f);
}