#include "PCH.h"
#include "Transform.h"


Transform::Transform()
	: m_scale(1.0f, 1.0f, 1.0f)
{
	CacheTransform();
}

Transform::Transform(const glm::vec3& position)
	: TransformNoScale(position)
	, m_scale(1.0f, 1.0f, 1.0f)
{
	CacheTransform();
}

Transform::Transform(const glm::vec3& position, const glm::quat& rotation)
	: TransformNoScale(position, rotation)
	, m_scale(1.0f, 1.0f, 1.0f)
{
	CacheTransform();
}

Transform::Transform(const glm::vec3& position, float pitch, float yaw, float roll)
	: TransformNoScale(position, pitch, yaw, roll)
	, m_scale(1.0f, 1.0f, 1.0f)
{
	CacheTransform();
}

Transform::Transform(const glm::vec3& position, const glm::quat& rotation, const glm::vec3& scale)
	: TransformNoScale(position, rotation)
	, m_scale(scale)
{
	CacheTransform();
}

bool operator==(const Transform& lhs, const Transform& rhs)
{
	return lhs.m_transformMatrix == rhs.m_transformMatrix;
}

bool operator!=(const Transform& lhs, const Transform& rhs)
{
	return lhs.m_transformMatrix != rhs.m_transformMatrix;
}

void Transform::SetScale(float byUniformScale)
{
	m_scale = { byUniformScale, byUniformScale, byUniformScale };
	CacheTransform();
}

void Transform::SetScale(const glm::vec3& newScale)
{
	m_scale = newScale;
	CacheTransform();
}

void Transform::SetScale(float x, float y, float z)
{
	m_scale = { x, y, z };
	CacheTransform();
}

void Transform::Scale(float uniformScale)
{
	m_scale.x *= uniformScale;
	m_scale.y *= uniformScale;
	m_scale.z *= uniformScale;
	CacheTransform();
}

void Transform::Scale(const glm::vec3& dScale)
{
	m_scale.x *= dScale.x;
	m_scale.y *= dScale.y;
	m_scale.z *= dScale.z;
	CacheTransform();
}

void Transform::Scale(float byX, float byY, float byZ)
{
	m_scale.x *= byX;
	m_scale.y *= byY;
	m_scale.z *= byZ;
	CacheTransform();
}

void Transform::ScaleX(float byX)
{
	m_scale.x *= byX;
	CacheTransform();
}

void Transform::ScaleY(float byY)
{
	m_scale.y *= byY;
	CacheTransform();
}

void Transform::ScaleZ(float byZ)
{
	m_scale.z *= byZ;
	CacheTransform();
}

void Transform::CacheTransform()
{
	// Translate -> Rotate -> Scale
	m_transformMatrix = glm::translate(glm::mat4(1.0f), m_position) * glm::toMat4(m_rotation) * glm::scale(glm::mat4(1.0f), m_scale);
}