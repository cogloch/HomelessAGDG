#include "PCH.h"
#include "EventBroadcaster.h"
#include "EventHandler.h"


bool EventBroadcaster::s_bInstantiated = false;

EventBroadcaster::EventBroadcaster()
	: m_eventStackTop(0)
{
	assert(!s_bInstantiated);
	s_bInstantiated = true;
}

EventBroadcaster::~EventBroadcaster()
{
	s_bInstantiated = false;
}

void EventBroadcaster::AddHandler(EventHandler* newHandler)
{
	for (const auto& handler : m_handlers)
	{
		if (handler == newHandler)
		{
			return;
		}
	}

	m_handlers.push_back(newHandler);
}

void EventBroadcaster::RemoveHandler(EventHandler* handler)
{
	for (auto it = m_handlers.begin(), end = m_handlers.end(); it != end; ++it)
	{
		if (*it == handler)
		{
			m_handlers.erase(it);
			return;
		}
	}
}

void EventBroadcaster::AddEvent(Event&& event, float delay)
{
	event.delay = delay;
	m_events[m_eventStackTop++] = event;
}

void EventBroadcaster::Broadcast(float dt)
{
	// TODO: Replace with insertion sort(disputable).
	std::sort(m_events, m_events + m_eventStackTop, [](Event& rhs, Event& lhs){ return rhs.delay > lhs.delay; });

	U32 broadcastStartIdx = 0;

	// Find the point where broadcastable events begin(and decrement delivery times on the way).
	for (; broadcastStartIdx < m_eventStackTop; ++broadcastStartIdx)
	{
		if ((m_events[broadcastStartIdx].delay -= dt) <= 0)
		{
			break;
		}
	}

	// The range [broadcastStartIdx..m_eventStackTop - 1] represents dispatchable events.
	for (auto i = broadcastStartIdx; i < m_eventStackTop; ++i)
	{
		for (auto& handler : m_handlers)
		{
			handler->Handle(m_events[i]);
		}
	}

	// Recycle broadcasted events.
	m_eventStackTop = broadcastStartIdx;
}
