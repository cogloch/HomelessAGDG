#pragma once
#include "Event.h"
#include "EventHandler.h"


using std::vector;
const U32 MAX_NUM_EVENTS = 200;

class EventBroadcaster final 
{
public:
	EventBroadcaster();
	~EventBroadcaster();
	
	void AddHandler(EventHandler*);
	void RemoveHandler(EventHandler*);

	void AddEvent(Event&&, float delay = 0.0f);
	void Broadcast(float dt);

private:
	EventBroadcaster(const EventBroadcaster&) = delete;                  // Singleton
	const EventBroadcaster& operator=(const EventBroadcaster&) = delete; // Singleton
	
	vector<EventHandler*> m_handlers;
	Event m_events[MAX_NUM_EVENTS];
	U32   m_eventStackTop;

	static bool s_bInstantiated;
};

extern EventBroadcaster g_eventBroadcaster;