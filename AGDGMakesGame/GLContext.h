#pragma once


class GLContext
{
public:
	GLContext();
	~GLContext();
	
	void Init(HWND, U32 width, U32 height, U32 majorVersion = 4, U32 minorVersion = 5);

	// TODO: Add clear flags
	void ClearBackBuffer();
	void ClearBackBuffer(float r, float g, float b, float a);
	void SwapContextBuffers();

	void Resize(U32 width, U32 height);

private:
	HWND  m_hWindow;
	HDC   m_hDeviceContext;
	HGLRC m_hRenderContext;

	static bool s_bLoadedFunctions;
	void LoadFunctions();

private:
	void CreatePixelFormat();
	void CreateContext(U32 majorVersion, U32 minorVersion);
	void DestroyContext();
};

struct GLFunctionLoadingError : std::runtime_error
{
	GLFunctionLoadingError(const std::string& libName)
		: std::runtime_error("Failed loading " + libName + " functions.\n") {}
};