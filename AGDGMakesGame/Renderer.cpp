#include "PCH.h"
#include "Renderer.h"
#include "ShaderGuard.h"
#include "Vertex.h"
#include "ResourceCache.h"


// Force Nvidia Optimus to use the dedicated GPU
extern "C" {
	_declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001;
}

#pragma comment(lib, "opengl32.lib")
#ifdef _DEBUG
#pragma comment(lib, "SOIL-d.lib")
#else
#pragma comment(lib, "SOIL.lib")
#endif

Renderer::Renderer()
	: m_pContext(nullptr)
	, m_projPerspective(800.0f, 600.0f, 45.0f)
	, m_projOrthographic(800.0f, 600.0f)
{
	TransformNoScale initialCamTransform(glm::vec3(0.0f, 0.0f, 3.0f));
	m_camera.Update(initialCamTransform);
}

void Renderer::Render()
{
	m_pContext->ClearBackBuffer(217.0f / 255.0f, 154.0f / 255.0f, 50.0f / 255.0f, 1.0f);

	// Scene
	auto viewProjMat = m_camera.GetViewProjMatrix(m_projPerspective);
	for (U32 i = 0; i < m_renderQueueEnd; i++)
	{
		m_renderItems[m_renderQueue[i]].Render(viewProjMat);
	}

	// UI
	auto projMat = m_projOrthographic.GetMatrix();
	for (U32 i = 0; i < m_renderQueueUIEnd; i++)
	{
		m_renderItemsUI[m_renderQueueUI[i]].Render(projMat);
	}

	m_pContext->SwapContextBuffers();
}

void Renderer::UpdateUI(const std::vector<UIElement>& uiElements)
{
	for (auto& cacheRequest : m_uiRenderItemCacheRequets)
	{
		m_renderItemsUI.push_back(RenderItemUI(cacheRequest.texture));
	}
	m_uiRenderItemCacheRequets.erase(m_uiRenderItemCacheRequets.begin(), m_uiRenderItemCacheRequets.end());

	m_renderQueueUIEnd = 0;
	for (auto& uiElement : uiElements)
	{
		auto itemId = uiElement.GetRenderItem();
		m_renderItemsUI[itemId].SetTransform(uiElement.GetTransformMatrix());
		m_renderQueueUI[m_renderQueueUIEnd++] = itemId;
	}
}

void Renderer::UpdateView(const TransformNoScale& camTransform)
{
	m_camera.Update(camTransform);
}

void Renderer::UpdateScene(const std::vector<RenderComponent>& renderComponents)
{
	for (auto& cacheRequest : m_renderItemCacheRequets)
	{
		m_renderItems.push_back(RenderItem(cacheRequest.shader, cacheRequest.mesh));
	}
	m_renderItemCacheRequets.erase(m_renderItemCacheRequets.begin(), m_renderItemCacheRequets.end());

	m_renderQueueEnd = 0;
	for (auto& component : renderComponents)
	{
		auto itemId = component.GetRenderItem();
		m_renderItems[itemId].SetTransform(component.m_transform.GetMatrix());
		m_renderQueue[m_renderQueueEnd++] = itemId;
	}
}

U32 Renderer::AddRenderItem(ResourceHandle shader, ResourceHandle mesh)
{
	m_renderItemCacheRequets.push_back({ shader, mesh });
	return m_renderItems.size() - 1 + m_renderItemCacheRequets.size();
}

U32 Renderer::AddUIRenderItem(ResourceHandle texture)
{
	m_uiRenderItemCacheRequets.push_back({ texture });
	return m_renderItemsUI.size() - 1 + m_uiRenderItemCacheRequets.size();
}

void Renderer::Init(GLContext* pContext)
{
	assert(pContext);
	assert(!m_pContext);
	
	m_pContext = pContext;
}