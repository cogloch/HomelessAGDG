#pragma once
#include "Transform.h"


class Renderer;

class UIElement
{
public:
	UIElement(Renderer*, const std::string& textureName);
	U32 GetRenderItem() const;
	const glm::mat4& GetTransformMatrix() const;

	// (0, 0) is the top-left corner
	void SetPosition(const glm::vec2&);
	void SetPosition(float x, float y);
	void SetRotation(float angleInDeg);
	void SetSize(float uniformScale);
	void SetSize(const glm::vec2&);
	void SetSize(float scaleX, float scaleY);

	void Translate(const glm::vec2&);
	void Translate(float x, float y);
	void Rotate(float angleInDeg);
	void Scale(float uniformScale);
	void Scale(const glm::vec2&);
	void Scale(float scaleX, float scaleY);

private:
	Transform m_transform;
	U32 m_renderItemId;
	U32 m_width;
	U32 m_height;

	static float s_offsetY;
};

inline U32 UIElement::GetRenderItem() const
{
	return m_renderItemId;
}

inline const glm::mat4& UIElement::GetTransformMatrix() const
{
	return m_transform.GetMatrix();
}