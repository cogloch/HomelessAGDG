#pragma once
#include "Projection.h"


class RenderItemUI
{
public:
	RenderItemUI(ResourceHandle texture);
	void Render(const glm::mat4& projMat) const;
	void SetTransform(const glm::mat4& transform);

private:
	glm::mat4 m_transform;
	ResourceHandle m_texture;
};