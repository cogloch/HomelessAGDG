#include "PCH.h"
#include "RenderComponent.h"
#include "Vertex.h"
#include "Renderer.h"
#include "Mesh.h"
#include "ResourceCache.h"


RenderComponent::RenderComponent(Renderer* pRenderer, const std::string& shaderName, const std::string& meshName)
{
	m_renderItemId = pRenderer->AddRenderItem(g_resourceCache.GetShaderHandle(shaderName), g_resourceCache.GetMeshHandle(meshName));
}