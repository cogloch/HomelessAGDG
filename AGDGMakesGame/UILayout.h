#pragma once
#include "UIElement.h"


class Renderer;

class UILayout
{
public:
	UILayout(Renderer*, const std::string& fullPath);
	UIElement CreateUIElement(Renderer*, const std::string& texture, U32 x, U32 y); 

	std::vector<UIElement> uiElements;
};