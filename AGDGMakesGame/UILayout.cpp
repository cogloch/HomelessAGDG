#include "PCH.h"
#include "UILayout.h"


UILayout::UILayout(Renderer* pRenderer, const std::string& fullPath)
{
	std::ifstream layout(fullPath);
	
	// Lots of dummy stuff temporarily 
	std::string token;
	std::string textureName;
	U32 x, y;
	while (layout >> token)
	{
		// First one is the element name/id that can be used with scripting
		// Second one is texture: textureName
		layout >> token >> textureName;
		// Position.x: x
		layout >> token >> token;
		x = std::stoi(token);
		// Position.y: y
		layout >> token >> token;
		y = std::stoi(token);
		uiElements.push_back(CreateUIElement(pRenderer, textureName, x, y));
	}
}

UIElement UILayout::CreateUIElement(Renderer* pRenderer, const std::string& texture, U32 x, U32 y)
{
	UIElement elem(pRenderer, texture);
	elem.SetPosition(x, y);
	//elem.Scale(0.5f);
	return elem;
}