#pragma once

#include <cstdint>

using U8 = std::uint8_t;
using U16 = std::uint16_t;
using U32 = std::uint32_t;
using U64 = std::uint64_t;

using I32 = std::int32_t;

// TEMP
using ResourceHandle = U32;