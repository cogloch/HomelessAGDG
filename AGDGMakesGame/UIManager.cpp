#include "PCH.h"
#include "Renderer.h"
#include "UIManager.h"


bool UIManager::s_bInstantiated = false;

UIManager::UIManager()
	: m_activeLayout(-1)
{
	assert(!s_bInstantiated);
	s_bInstantiated = true;
}

void UIManager::Init(Renderer* pRenderer, const std::string& fullLayoutsPath)
{
	m_pRenderer = pRenderer;
	std::ifstream layoutList(fullLayoutsPath + "LayoutList.txt");

	std::string line;
	while (std::getline(layoutList, line))
	{
		// Lines starting in # are comments
		if (line[0] != '#')
		{
			m_layouts.push_back(UILayout(m_pRenderer, fullLayoutsPath + line));
		}
	}
}

bool bDrag = false;

void UIManager::Handle(const Event& ev)
{
	switch (ev.type)
	{
	case EventType::LMOUSE_UP:
		OutputDebugStringA("Left mouse up\n");
		bDrag = false;
		break;
	case EventType::LMOUSE_DOWN:
		bDrag = true;
		break;
	case EventType::RMOUSE_DOWN:
		OutputDebugStringA("Right mouse down\n");
		break;
	case EventType::MOUSE_WHEEL:
		OutputDebugStringA("Scroll\n");
		break;
	case EventType::MOUSE_MOVE:
	{
		if (bDrag)
		{
			m_layouts[m_activeLayout].uiElements[0].SetPosition(ev.data[0].asU32, ev.data[1].asU32);
		}
		break;
	}
	}
}