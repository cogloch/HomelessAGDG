#pragma once
#include "ShaderProgram.h"
#include <glm\glm.hpp>
#include <glm\gtc\type_ptr.hpp>


// ShaderProgram RAII wrapper
// Usage is weird and ugly, but works.
// TODO: Make usage not weird and ugly.
class ShaderGuard
{
public:
	ShaderGuard(const ShaderProgram&);

	// Gets the shader program with the passed name from ResourceManager
	ShaderGuard(const std::string&);

	// Pass an Opengl shader program name
	// Which shouldn't be stored ouside ResourceManager in the first place 
	// Meaning this may go away
	ShaderGuard(const U32);
	~ShaderGuard();

	// Use non-cached uniform locations.
	void SetUniform(const std::string&, float) const;
	void SetUniform(const std::string&, float, float) const;
	void SetUniform(const std::string&, float, float, float) const;
	void SetUniform(const std::string&, float, float, float, float) const;

	void SetUniform(const std::string&, I32) const;
	void SetUniform(const std::string&, I32, I32) const;
	void SetUniform(const std::string&, I32, I32, I32) const;
	void SetUniform(const std::string&, I32, I32, I32, I32) const;

	void SetUniform(const std::string&, const glm::vec2&) const;
	void SetUniform(const std::string&, const glm::vec3&) const;
	void SetUniform(const std::string&, const glm::vec4&) const;

	void SetUniform(const std::string&, const glm::mat2&, GLboolean transpose = false) const;
	void SetUniform(const std::string&, const glm::mat3&, GLboolean transpose = false) const;
	void SetUniform(const std::string&, const glm::mat4&, GLboolean transpose = false) const;

	// Use cached uniform locations.
	void SetUniform(I32 location, float) const;
	void SetUniform(I32 location, float, float) const;
	void SetUniform(I32 location, float, float, float) const;
	void SetUniform(I32 location, float, float, float, float) const;

	void SetUniform(I32 location, I32) const;
	void SetUniform(I32 location, I32, I32) const;
	void SetUniform(I32 location, I32, I32, I32) const;
	void SetUniform(I32 location, I32, I32, I32, I32) const;

	void SetUniform(I32 location, const glm::vec2&) const;
	void SetUniform(I32 location, const glm::vec3&) const;
	void SetUniform(I32 location, const glm::vec4&) const;

	void SetUniform(I32 location, const glm::mat2&, GLboolean transpose = false) const;
	void SetUniform(I32 location, const glm::mat3&, GLboolean transpose = false) const;
	void SetUniform(I32 location, const glm::mat4&, GLboolean transpose = false) const;

	// Used to cache uniform locations.
	I32 GetUniformLocation(const std::string&) const;

public:
	ShaderGuard(ShaderGuard&&) = delete;
	ShaderGuard(const ShaderGuard&) = delete;
	ShaderGuard& operator = (ShaderGuard&&) = delete;
	ShaderGuard& operator = (const ShaderGuard&) = delete;

private:
	U32 m_oglName;

	// Check if there's another bound shader program object, meaning that someone, somewhere, out there
	// isn't using this. Logs the occurence.
	void AssertNoShaderBound();
};

struct ShaderBindingError : std::runtime_error
{
	ShaderBindingError(const std::string& errorMsg) 
		: std::runtime_error("Shader binding error: " + errorMsg + "\n") {}
};