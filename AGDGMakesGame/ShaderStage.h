#pragma once


enum class ShaderType
{
	COMPUTE      = gl::COMPUTE_SHADER,
	VERTEX       = gl::VERTEX_SHADER,
	TESS_CONTROL = gl::TESS_CONTROL_SHADER,
	TESS_EVAL    = gl::TESS_EVALUATION_SHADER,
	GEOMERTRY    = gl::GEOMETRY_SHADER,
	FRAGMENT     = gl::FRAGMENT_SHADER
};

class ShaderStage
{
public:
	ShaderStage(const std::string& name, ShaderType type, U32 program);
	~ShaderStage();

	ShaderStage(ShaderStage&&);

	// Having a reference to an Opengl object as member which is released on deletion.
	ShaderStage(const ShaderStage&) = delete;
	ShaderStage& operator=(const ShaderStage&) = delete;

	U32 GetShaderObject() const;

	static std::map<ShaderType, std::string> s_GLSLExtensions;

private:
	void ReadShaderSource(const std::string& filename);
	void CreateShader();

private:
	// Name of the shader program this is being linked into
	const std::string m_name;
	const ShaderType m_type;

	// Reference to the program object which this shader is being linked into
	const U32 m_program;

	// Reference to a shader object
	U32 m_shader;

	// Shader source code
	std::string m_source;
};

inline U32 ShaderStage::GetShaderObject() const
{
	return m_shader;
}

struct ShaderCompilationError : std::runtime_error
{
	ShaderCompilationError(const std::string& shaderName, ShaderType shaderType, const std::string& errorMsg)
		: std::runtime_error("Error compiling shader: " + shaderName + ShaderStage::s_GLSLExtensions[shaderType] + " - " + errorMsg + "\n") {}
};

struct FileNotFound : std::runtime_error
{
	FileNotFound(const std::string& filePath)
		: std::runtime_error("File not found: " + filePath + "\n") {}
};