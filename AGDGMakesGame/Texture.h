#pragma once


class Texture
{
public:
	Texture()
	{
	}

	void Init(U32 width, U32 height, const void* pixels)
	{
		this->width = width;
		this->height = height;

		gl::GenTextures(1, &texture);
		gl::BindTexture(gl::TEXTURE_2D, texture);
		gl::TexImage2D(gl::TEXTURE_2D, 0, gl::RGB, width, height, 0, gl::RGB, gl::UNSIGNED_BYTE, pixels);
		gl::GenerateMipmap(gl::TEXTURE_2D);
		gl::BindTexture(gl::TEXTURE_2D, 0);
	}

	U32 texture;
	U32 width;
	U32 height;
};