#pragma once
#include "Event.h"
#include <utility>


Event&& CreateLMouseUpEv(U32 positionX, U32 positionY)
{
	Event ev;
	ev.type = EventType::LMOUSE_UP;
	ev.length = 2;
	ev.data[0] = EventData(positionX);
	ev.data[1] = EventData(positionY);
	
	return std::move(ev);
}

Event&& CreateRMouseUpEv(U32 positionX, U32 positionY)
{
	Event ev;
	ev.type = EventType::RMOUSE_UP;
	ev.length = 2;
	ev.data[0] = EventData(positionX);
	ev.data[1] = EventData(positionY);

	return std::move(ev);
}

Event&& CreateLMouseDownEv(U32 positionX, U32 positionY)
{
	Event ev;
	ev.type = EventType::LMOUSE_DOWN;
	ev.length = 2;
	ev.data[0] = EventData(positionX);
	ev.data[1] = EventData(positionY);

	return std::move(ev);
}

Event&& CreateRMouseDownEv(U32 positionX, U32 positionY)
{
	Event ev;
	ev.type = EventType::RMOUSE_DOWN;
	ev.length = 2;
	ev.data[0] = EventData(positionX);
	ev.data[1] = EventData(positionY);

	return std::move(ev);
}

Event&& CreateMouseMoveEv(U32 positionX, U32 positionY)
{
	Event ev;
	ev.type = EventType::MOUSE_MOVE;
	ev.length = 2;
	ev.data[0] = EventData(positionX);
	ev.data[1] = EventData(positionY);

	return std::move(ev);
}

Event&& CreateMouseWheelEv(U32 delta)
{
	Event ev;
	ev.type = EventType::MOUSE_WHEEL;
	ev.length = 1;
	ev.data[0] = EventData(delta);

	return std::move(ev);
}