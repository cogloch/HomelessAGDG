#pragma once


// Angles are in degrees
class TransformNoScale
{
public:
	TransformNoScale();
	TransformNoScale(const glm::vec3& position);
	TransformNoScale(const glm::vec3& position, const glm::quat& rotation);
	TransformNoScale(const glm::vec3& position, float pitch, float yaw, float roll);

	const glm::quat& GetRotationQuat() const;
	const glm::vec3& GetPosition() const;
	const glm::mat4& GetMatrix() const;

	// TODO: Transform concatenation

	friend bool operator==(const TransformNoScale&, const TransformNoScale&);
	friend bool operator!=(const TransformNoScale&, const TransformNoScale&);

	// Absolute values
	void SetPosition(const glm::vec3&);
	void SetPosition(float x, float y, float z);

	void SetRotation(const glm::mat4& fromRotMatrix);
	void SetRotation(const glm::vec3&);
	void SetRotation(float pitchAngle, float yawAngle, float rollAngle);
	void SetRotation(const glm::quat&);

	// Relative values
	void Translate(const glm::vec3&);
	void Translate(float dX, float dY, float dZ);
	void TranslateX(float dX);
	void TranslateY(float dY);
	void TranslateZ(float dZ);

	void Rotate(const glm::quat&);
	void RotateX(float dPitchAngle);
	void RotateY(float dYawAngle);
	void RotateZ(float dRollAngle);

protected:
	virtual void CacheTransform();
	glm::mat4 m_transformMatrix;
	glm::vec3 m_position;
	glm::quat m_rotation;
};

const glm::vec3 xAxis = { 1.0f, 0.0f, 0.0f },
				yAxis = { 0.0f, 1.0f, 0.0f },
				zAxis = { 0.0f, 0.0f, 1.0f };

// TODO: Move inlined funcs in a separate file
inline const glm::quat& TransformNoScale::GetRotationQuat() const
{
	return m_rotation;
}

inline const glm::vec3& TransformNoScale::GetPosition() const
{
	return m_position;
}

inline const glm::mat4& TransformNoScale::GetMatrix() const
{
	return m_transformMatrix;
}